package com.gotrade.apirepeatrequest.controller;

import com.gotrade.apirepeatrequest.annotation.NoRepeatSubmission;
import com.gotrade.apirepeatrequest.model.Result;
import com.gotrade.apirepeatrequest.model.User;
import org.springframework.web.bind.annotation.*;

/**
 * @author jason.tang
 * @create 2019/4/8
 * @description
 */

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping({"", "/"})
    public Result<String> getUser() {
        return Result.success("Jason");
    }

    @NoRepeatSubmission
    @GetMapping("/{userNum}")
    public Result<String> getUser(@PathVariable Integer userNum) {
        String str = "";
        if (userNum == 1) {
            str = "Jason";
        } else if (userNum == 2) {
            str = "Rose";
        } else {
            str = "Other";
        }
        return Result.success(str);
    }

    @NoRepeatSubmission
    @PostMapping({"", "/{companyID}"})
    public Result<User> addUser(@PathVariable String companyID, @RequestBody User user) {
        return Result.success(user);
    }

    @NoRepeatSubmission
    @PutMapping("/{userNum}")
    public Result<User> modifyUser(@PathVariable Integer userNum, @RequestBody User user) {
        return Result.success(user);
    }
}
