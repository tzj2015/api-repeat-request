package com.gotrade.apirepeatrequest.model;

import lombok.*;

/**
 * @author jason.tang
 * @create 2019/4/8
 * @description API请求的回复信息类
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    private static final String SUCCESS = "200";
    private static final String FAILURE = "500";

    private String code;

    private String message;

    private T data;

    /**
     * 请求成功，没有实体信息返回，只返回成功状态
     * @param <T>
     * @return
     */
    public static <T> Result<T> success() {
        Result<T> result = new Result<T>();
        result.setCode(SUCCESS);
        result.setMessage("success");
        return result;
    }

    /**
     * 请求成功，返回实体信息
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<T>();
        result.setCode(SUCCESS);
        result.setMessage("success");
        result.setData(data);
        return result;
    }

    /**
     * 出现错误，只返回错误信息
     * @param <T>
     * @return
     */
    public static <T> Result<T> failure() {
        Result<T> result = new Result<T>();
        result.setCode(FAILURE);
        result.setMessage("failure");
        return result;
    }

    /**
     * 出现错误，返回错误信息和实体信息
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> failure(T data) {
        Result<T> result = new Result<T>();
        result.setCode(FAILURE);
        result.setData(data);
        result.setMessage("failure");
        return result;
    }
}
